all: index.html

index.html: templates/index.md
	pandoc -s --template "templates/layout" -f markdown -t html5 -o "$@" "$<"
